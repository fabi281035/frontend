import { Offer } from "./offer";
import { Media } from "./userProfile";

export type Review = {
  id: number;
  description: string;
  rate: number;
  owner: {
    id: number;
    name: string;
    lastName: string;
    profileImage: {
      media: Media
    };
  };
};

export interface Product {
    id: number;
    name: string;
    description?: null;
    quantity: number;
    wholesale: number;
    price: number;
    rate: number;
    brand: Brand;
    mainPhoto: IconOrMainPhoto;
    productDetails?: (ProductDetail)[] | null;
    productGallery?: null;
    productCategory: ProductCategory;
    offers?: Offer[] ;
    hasOffer?: boolean;
    totalOffers?: number
    priceAfterOffer?: number
    isFavorite?: boolean,
    productReviews: Review []
  }

  export interface ProductDetail {
    id: number,
    media: Media,
    color: string,
    size: string[],
    price: number | null,
    quantity: number | null,
    wholesale: number | null
  }

  export interface Brand {
    id?: number;
    name: string;
    logo: Logo;
    productCount?: number;
  }
  export interface Logo {
    id: number;
    userId: number;
    type: string;
    size: number;
    url: string;
    galleryId?: null;
    profileImageId?: null;
    productGalleryId?: null;
  }
  export interface IconOrMainPhoto {
    id: number;
    url: string;
    size: number;
    type: string;
  }
  export interface ProductCategory {
    id: number;
    name: string;
    icon: IconOrMainPhoto;
  }

  export interface AddReviewToProduct {
    userId:number,
    rate:number,
    description: string,
    productId: number
  }
  

  export  enum productFavoredChangeStatus{
    ADD="add",
    DELETE="delete"
  }