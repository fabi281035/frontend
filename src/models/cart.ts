import { Address } from "./address"
import { ProductCart } from "./productCart"

export interface ProductsCart {
    id: number
    product: ProductCart
    color: string
    size: string
    quantity: number
    productPrice: number
  }
  
  export enum CartStatus {
    INPROGRESS = 'INPROGRESS',
    REJECTED = 'REJECTED',
    ONWAY = 'ONWAY' ,
    DELIVERED = 'DELIVERED'
  }
  
  
  export interface CartList {
    id: number 
    number: string 
    createdAt: Date 
    status: CartStatus
    totalPrice: number
    deliveryCost: number
    productCarts: ProductsCart[]
    address: Address
  }
  