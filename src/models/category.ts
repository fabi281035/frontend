import { Brand } from "./product";

export interface Category {
    icon: Icon;
    id: number;
    name: string;
  }

  export interface CategoryWithCountProductInBrand {
    category: Category
    brand: Brand
    productCount: number
  }
  export interface Icon {
    id: number;
    userId: number;
    type: string;
    size: number;
    url: string;
    galleryId?: null;
    profileImageId?: null;
    productGalleryId?: null;
  }
  