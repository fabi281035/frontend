export interface LoginForm{
    email: string,
    password: string,
    name: string,
    lastName: string,
    gender: string,
    phone: string,
    birthday: Date,
  }