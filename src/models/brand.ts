import { Offer, OfferWithProducts } from "./offer";
import { Product, ProductDetail } from "./product";

interface brand{
    id:string,
    name:string,
    numOfProducts:string,
}
export interface BrandVM {
    id: number;
    name: string;
    logo: Logo;
    products: Product[];
    productByCategory: Product[][];
    offers: OfferWithProducts[]
  }
  export interface Logo {
    id: number;
    userId: number;
    type: string;
    size: number;
    url: string;
    galleryId?: null;
    profileImageId?: null;
    productGalleryId?: null;
  }
  export interface ProductsEntity {
    id: number;
    name: string;
    description?: null;
    quantity: number;
    wholesale: number;
    price: number;
    rate: number;
    brand: Brand;
    mainPhoto: IconOrMainPhoto;
    productDetails?: (ProductDetail)[] | null;
    productGallery?: null;
    productCategory: ProductCategory;
    offers?: OfferWithProducts[] | null;
  }
  export interface Brand {
    name: string;
    logo: Logo;
  }
  export interface IconOrMainPhoto {
    id: number;
    url: string;
    size: number;
    type: string;
  }
  export interface ProductCategory {
    name: string;
    icon: IconOrMainPhoto;
  }
  