interface specialOffer {
    id:string,
    imgUrl:string,
    offerText:string,
    offerPrice:number,
    hasShippingExpenses:boolean,
    savingsPercentage:number,
    originalPrice?:number,
}