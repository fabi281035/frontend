export enum WholesaleRequestType {
  VENDER = 'VENDER',
  BUYER = 'BUYER',
}
export interface WholesaleRequest {
  brandName: string,
  brandNameOwnerName: string,
  address: string,
  phoneNumber: string,
  branchesNumber: number,
  type: WholesaleRequestType
}
