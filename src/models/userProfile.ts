export type Media = {
  id: number
  url: string
  title: string
  size?: number
  type: 'IMAGE' | 'VEDIO'
}

export interface userProfile {
    id: number,
    name: string,
    lastName?: string,
    gender: "MALE" | "FEMALE",
    phone: string,
    birthday: Date,
    rate: number,
    profileImage: {
      media: Media
    } ,
    email: string
    mediaId?:string
  }