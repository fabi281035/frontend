import { Product } from "./product"

export interface Offer {
    brandId?: Number
    description?: string
    expiryDate: Date
    id: number
    name: string
    percentOffer: number
  } 


export interface OfferWithProducts {
    id: number,
    name: string,
    description?: string,
    expiryDate: Date
    products: Product[]
}