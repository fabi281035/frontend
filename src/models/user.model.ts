export interface userVM {
   id:number, 
    name: string,
    lastName: string,
    gender: string,
    phone: string,
    birthday: Date
  }
