import { userProfile } from "./userProfile"

export interface AffiliateMarketingCode {
  id:number
  code: string
  codeUsedCount: number
  userBalance: number
  }
