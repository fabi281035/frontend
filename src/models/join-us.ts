export interface JoinUs{
    brandName:string,
    brandOwnerName:string,
    phone:string,
    branchNumber:number,
    address:string
}