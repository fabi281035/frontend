export interface ProductCart {
    id:number
    name:string
    price:number
    imageUrl:string
    quantity: number
    color: string
    size: string
    totalPrice?: number
    mainPhoto?: {
      id: number,
      url: string,
      size: string,
      type: string
    }
}

export const defaultProductCart: ProductCart = {
    color:'',
    id: 0,
    imageUrl:'https://www.dcgpac.com/media/catalog/product/cache/d34276f40b5b4d846d90480d726c8ec8/7/_/7.01x4.02_1_1.jpg',
    name:'',
    price: 0,
    quantity: 0,
    size: '',
    totalPrice: 0
  } 

  export const getDefaultProductCart = () =>{
	return JSON.parse( JSON.stringify( defaultProductCart ) );

}

