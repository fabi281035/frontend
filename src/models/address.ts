export interface Address {
    id: number
    name: string
    city: string
    country: string
    description: string
}


export interface Country { 
    code: string,
    name: string,
    dialCode: string,
    cities?: {
        name:string
    }[]
}