// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    // apiUrl:'https://api-fabi.cyclic.cloud/',
    // apiUrl:'http://localhost:3000',
    apiUrl:'http://138.68.85.3:3000',
    frontUrl:'http://localhost:4200',
    // firebaseConfig : {
    //   apiKey: "AIzaSyBkVxbGO0FAuC59VpN09zP-8J1my2rBKYw",
    //   authDomain: "sata-312021.firebaseapp.com",
    //   projectId: "sata-312021",
    //   storageBucket: "sata-312021.appspot.com",
    //   messagingSenderId: "841673574790",
    //   appId: "1:841673574790:web:cfe3d9fb4718de7bad4407",
    //   measurementId: "G-VCGQERH6LH"
    // }
  };
  