import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { userProfile } from 'src/models/userProfile';
import { UserAddress } from '../main/cart/payment-cart/payment-cart.component';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient,) { }

  getUserProfile() {
    return this.http.get<userProfile>(`${this.apiUrl}/user/profile`)
  }

  updateUserProfile(userProfile:userProfile){
    return this.http.put(`${this.apiUrl}/user`,userProfile)
  }

  getUserAddress (){
    return this.http.get<UserAddress[]>(`${this.apiUrl}/user/addresses`)
  }
}
