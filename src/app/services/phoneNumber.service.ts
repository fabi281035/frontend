import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AbstractControl, ValidatorFn } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class PhoneNumberService {


  constructor() { }

    phoneValidator(): ValidatorFn {
        return (control: AbstractControl): {[key: string]: any} | null => {
            const valid = /^(00963|\+963|09)\d{6,9}$/.test(control.value);
            return valid ? null : {'invalidPhone': {value: control.value}};
        };
    }
}
