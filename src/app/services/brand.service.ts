import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BrandVM } from 'src/models/brand';
import { Brand } from 'src/models/product';

@Injectable({
  providedIn: 'root'
})
export class BrandService {
  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient,) { }

  getBrands() {
    return this.http.get<Brand[]>(`${this.apiUrl}/brand/all`);
  }

  getBrandById(brandId:string) {
    return this.http.get<BrandVM>(`${this.apiUrl}/brand/${brandId}`);
  }
}
