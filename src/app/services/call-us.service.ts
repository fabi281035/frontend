import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { CallUs } from "src/models/call-us";

@Injectable({
  providedIn: 'root'
})
export class CallUsService {
    constructor(private http:HttpClient,
    ){}

    url = environment.apiUrl

    createMessage(callUsMessage:CallUs){
        console.log("callUsMessage",callUsMessage)
        return this.http.post<CallUs>(`${this.url}/call-us`, callUsMessage)
    }

   
}