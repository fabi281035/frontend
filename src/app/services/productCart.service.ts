import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { CartList } from "src/models/cart";
import { ProductCart } from "src/models/productCart";

export interface CartPost {
    products: 
        {
          productId: number,
          quantity: number,
          size: string,
          color: string,
          price: number
        }[] | undefined
    addressId: number
}
@Injectable({
    providedIn: 'root'
  })
export class ProductCartService {

    private apiUrl = environment.apiUrl;
    constructor(private http: HttpClient) { }
  
    createCart(cartToInsert: CartPost){
        return this.http.post<{id:string}>(`${this.apiUrl}/cart`,cartToInsert)
    }

    getCurrentCart(){
        return this.http.get(`${this.apiUrl}/cart/current`).subscribe()
    }

    getAllCarts(){
        return this.http.get<CartList[]>(`${this.apiUrl}/cart/all`)
    }
}  