import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { CallUs } from "src/models/call-us";
import { JoinUs } from "src/models/join-us";

@Injectable({
  providedIn: 'root'
})
export class JoinUsService {
    constructor(private http:HttpClient,
    ){}

    url = environment.apiUrl
//send application to join us 
    joinUsSubmit(joinUsForm:JoinUs){
        console.log(joinUsForm)
        return this.http.post<JoinUs>(`${this.url}/join-us`, joinUsForm)
    }

   
}