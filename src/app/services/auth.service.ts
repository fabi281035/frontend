import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, take, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LoginForm } from 'src/models/loginForm.model';
import { userVM } from 'src/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  
  currentUser = new BehaviorSubject<userVM | null>(null)
  url = environment.apiUrl
  constructor(
    private http: HttpClient,
    private router:Router,
  ) { 
    this.checkLocalStorage();
  }

  login(email: string, password: string) {
    return this.http.post<{ token: string, user: userVM }>(`${this.url}/auth/login`, { email, password }).pipe(
      tap(({ user }) => {this.currentUser.next(user);}),
      tap(({ user }) => localStorage.setItem('user', JSON.stringify( user))),
      tap(({ token }) => localStorage.setItem('token', token))
    )
  }

  logout() {
    // const cart = this.injector.get<CartService>(CartService)
    this.currentUser.next(null)
    // cart.cartProducts$.next([])
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    this.router.navigate([''])
  }
  checkUser(id : string){
    this.http.get<boolean>(`${this.url}/user/check/${id}`).pipe(
      take(1),
      tap(res=>{
        if(!res){
          this.logout();
        }
      })
    ).subscribe()
  }

  singIn(logInForm:LoginForm){
    return this.http.post<{ token: string, user: userVM }>(`${this.url}/auth/register`, logInForm)
}

private checkLocalStorage() {
  const userString = localStorage.getItem('user');
  if (userString) {
    const user = JSON.parse(userString);
    this.currentUser.next(user);
  }
}

}
