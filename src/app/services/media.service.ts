import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MediaService {
  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient,) { }

  postMedia(file: File) {
      const formData: FormData = new FormData();
      formData.append('file', file, file.name);
      return this.http.post(`${this.apiUrl}/media`, formData);
  }
}
