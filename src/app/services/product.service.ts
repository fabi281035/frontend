import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BrandVM } from 'src/models/brand';
import { AddReviewToProduct, Product, productFavoredChangeStatus } from 'src/models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  public productFavoredChange = new BehaviorSubject<{product:Product,changeStatus:productFavoredChangeStatus}|undefined>(undefined)
  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient,) { }

  getProducts() {
    return this.http.get<Product[]>(`${this.apiUrl}/user/product/filter`);
  }
  getProductsFavorite() {
    return this.http.get<Product[]>(`${this.apiUrl}/user/product/favored`);
  }
  getProductsByCategoryId(categoryId:string) {
    return this.http.get<Product[]>(`${this.apiUrl}/user/product/byCategory/${categoryId}`);
  }
  getProductsByBrandId(brandId:string) {
    return this.http.get<BrandVM>(`${this.apiUrl}/brand/${brandId}`);
  }
  getNewProducts() {
    return this.http.get<Product[]>(`${this.apiUrl}/user/product/new`);
  }
  getProductsById(productId:string) {
    return this.http.get<Product>(`${this.apiUrl}/user/product/${productId}`);
  }
  getProductsByCategoryIdAndBrandId(categoryId:string, brandId:string) {
    return this.http.get<Product[]>(`${this.apiUrl}/user/product/category/${categoryId}/brand/${brandId}`);
  }

  getProductsBySearchKey(key:string) {
    return this.http.get<Product[]>(`${this.apiUrl}/user/product/search/${key}`);
  }

  getMostSellingProduct() {
    return this.http.get<Product[]>(`${this.apiUrl}/user/product/most-selling`);
  }
  
  postAddReview(reviewData:AddReviewToProduct){
    return this.http.post(`${this.apiUrl}/product-review`,reviewData)
  }
  addProductToFavorite(productId:number){
    return this.http.post(`${this.apiUrl}/user/product/makeFavored/${productId}`,{productId:productId});
  }
  deleteProductFromFavorite(productId:number){
    return this.http.delete(`${this.apiUrl}/user/product/deleteFavored/${productId}`);
  }
  getProductFromFavorite(productId:number){
    return this.http.get(`${this.apiUrl}/user/product/getFavored/${productId}`);
  }
}
