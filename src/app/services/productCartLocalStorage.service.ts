import { Injectable } from "@angular/core";
import { ProductCart } from "src/models/productCart";

@Injectable({
    providedIn: 'root'
  })
export class ProductCartLocalStorageService {

    constructor(){
    }

    addProductToCart(productToAdd:ProductCart){
        let cart: ProductCart[] = this.getCartFromLocalStorage()
        if(cart.find(product=> product.id == productToAdd.id)) return
        cart.push(productToAdd)
        localStorage.removeItem('cart') 
        localStorage.setItem('cart',JSON.stringify(cart)) 
    }

    removeProductsFromCart(){
        localStorage.removeItem('cart')
    }

    removeProductToCart(productToDelete:ProductCart){
        let cart: ProductCart[] = this.getCartFromLocalStorage()
        cart = cart.filter(product => product.id != productToDelete.id)
        localStorage.removeItem('cart') 
        localStorage.setItem('cart',JSON.stringify(cart)) 
    }

    getCartFromLocalStorage() {
        return JSON.parse(localStorage.getItem('cart') ?? '[]')
    }

    updateProductInCurrentCart(productToUpdate:ProductCart){
        let cart: ProductCart[] = this.getCartFromLocalStorage()
        cart = cart.filter(product => product.id != productToUpdate.id)
        cart.push(productToUpdate)
        localStorage.removeItem('cart') 
        localStorage.setItem('cart',JSON.stringify(cart)) 
    }

}  