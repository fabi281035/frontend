import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Category, CategoryWithCountProductInBrand } from 'src/models/category';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient,) { }

  getCategory() {
    return this.http.get<Category[]>(`${this.apiUrl}/category`);
  }

  getCategoryWithProductCountInBrand(categoryId:string,brandId:string){
    return this.http.get<CategoryWithCountProductInBrand>(`${this.apiUrl}/category/${categoryId}/brand/${brandId}`);
  }
}
