import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { NEVER, catchError } from "rxjs";
import { environment } from "src/environments/environment";
import { Address } from "src/models/address";

@Injectable({
  providedIn: 'root'
})
export class AddressService {
    constructor(private http:HttpClient,
    ){}

    url = environment.apiUrl

    createNewAddress(addressToCreate:Address){
        return this.http.post<Address>(`${this.url}/user/address`, addressToCreate)
    }

    getCurrentUserAddress(){
        return this.http.get<Address[]>(`${this.url}/user/addresses`)
    }

    deleteUserAddressById(addressId:number){
        return this.http.delete(`${this.url}/user/address/${addressId}`)
    }
}