import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { OfferWithProducts } from "src/models/offer";

@Injectable({
    providedIn: 'root'
})
export class ProductOfferService {
    private apiUrl = environment.apiUrl;
      constructor(private http: HttpClient,) { }

    getNewestOffer(){
      return this.http.get<OfferWithProducts[]>(`${this.apiUrl}/offer/newest`);
    }
}