import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AffiliateMarketingCode } from 'src/models/AffiliateMarketing.model';
import { BrandVM } from 'src/models/brand';
import { Brand } from 'src/models/product';

@Injectable({
  providedIn: 'root'
})
export class AffiliateMarketingService {
  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient,) { }

  fetchAffiliateMarketing() {
    return this.http.post<AffiliateMarketingCode>(`${this.apiUrl}/affiliate-marketing/code`,{});
  }
}
