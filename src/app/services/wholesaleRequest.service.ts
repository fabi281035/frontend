import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { firstValueFrom } from "rxjs";
import { environment } from "src/environments/environment";
import { WholesaleRequest } from "src/models/wholesaleRequest.model";

@Injectable({
  providedIn: 'root'
})
export class WholesaleRequestService {
    constructor(private http:HttpClient,
    ){}

    url = environment.apiUrl

    createNewWholesaleRequest(wholesaleRequest:WholesaleRequest){
        firstValueFrom (this.http.post(`${this.url}/wholesale-request`, {...wholesaleRequest,branchesNumber: +wholesaleRequest.branchesNumber}))
    }
}