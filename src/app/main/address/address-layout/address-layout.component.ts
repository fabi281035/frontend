import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Address, Country } from 'src/models/address';
import { countries } from '../dataSites/countries';
import { AddressService } from 'src/app/services/address.service';
import { Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-kids-layout',
  templateUrl: './address-layout.component.html',
  styleUrls: ['./address-layout.component.scss'],
})
export class AddressLayoutComponent implements OnInit {
  public addressForm: FormGroup = new FormGroup({
    name: new FormControl(''),
    city: new FormControl(''),
    country: new FormControl(''),
    description: new FormControl(''),
  });
  countries = countries;
  countrySelected: Country = countries[0];

  constructor(
    private addressService: AddressService,
    private snackBar: MatSnackBar
  ) {}

  private subscription = new Subscription();
  currentUserAddresses$ = this.addressService.getCurrentUserAddress();
  currentUserAddresses: Address[] = [];
  ngOnInit() {
    this.subscription.add(
      this.currentUserAddresses$.subscribe((addresses) => {
        this.currentUserAddresses = addresses;
      })
    );
  }

  onChangeCountry(event: any) {
    this.countrySelected = event.value;
  }

  addNewAddress() {
    const currentAddress = this.addressForm.getRawValue();
    this.subscription.add(
      this.addressService
        .createNewAddress({
          ...currentAddress,
          city: currentAddress.city.name,
          country: currentAddress.country.name,
        })
        .subscribe(
          (addressCreated:Address) => {
          this.snackBar.open('تم إضافة العنوان بنجاح', 'حسناً', {
            duration: 5000,
          });
          this.currentUserAddresses.unshift(addressCreated);
          this.clearAddressForm();
        })
    );
  }
  clearAddressForm() {
    this.addressForm.reset();
  }
}
