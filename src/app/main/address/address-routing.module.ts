import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddressLayoutComponent } from './address-layout/address-layout.component';

const routes: Routes = [{
  path:'',
  component: AddressLayoutComponent,
}];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddressRoutingModule { }
