import { Component, Input } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NEVER, Subscription, catchError } from 'rxjs';
import { AddressService } from 'src/app/services/address.service';
import { Address } from 'src/models/address';

@Component({
  selector: 'app-address-card',
  templateUrl: './address-card.component.html',
  styleUrls: ['./address-card.component.scss']
})
export class AddressCardComponent {

  @Input() address: Address | undefined;
  displayAddress = true
  private subscription = new Subscription();

  constructor(private addressService: AddressService,private snackBar: MatSnackBar){}

  deleteAddress(){
    this.subscription.add(this.addressService.deleteUserAddressById(this.address?.id!)
    .pipe(
      catchError(() => {
              this.snackBar.open('لا يمكنك حذف هذا العنوان, يوجد فاتورة متعلقة به', 'حسناً', {
                duration: 5000, 
              });
          return NEVER
      })
  )
    .subscribe(()=>{
      this.snackBar.open('تم حذف العنوان بنجاح', 'حسناً', {
        duration: 5000,
      });
      this.displayAddress = false

    }))
  }
  
}
