import { Component } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-kids-layout',
  templateUrl: './kids-layout.component.html',
  styleUrls: ['./kids-layout.component.scss']
})
export class KidsLayoutComponent {
  public products$ = this.productService.getProductsByCategoryId('3');

  constructor(private productService: ProductService) { }
}