import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { KidsLayoutComponent } from './kids-layout/kids-layout.component';

const routes: Routes = [{
  path:'',
  component: KidsLayoutComponent,
}];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KidsRoutingModule { }
