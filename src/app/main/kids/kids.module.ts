import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KidsRoutingModule } from './kids-routing.module';
import { KidsLayoutComponent } from './kids-layout/kids-layout.component';
import { ProductCardModule } from 'src/app/custom-components/product-card/product-card.module';


@NgModule({
  declarations: [
    KidsLayoutComponent
  ],
  imports: [
    CommonModule,
    KidsRoutingModule,
    ProductCardModule,
  ]
})
export class KidsModule { }
