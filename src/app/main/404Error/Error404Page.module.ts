import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import {MatCardModule} from '@angular/material/card'; 
// import {MatIconModule} from '@angular/material/icon';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatInputModule } from '@angular/material/input';
// import { ReactiveFormsModule } from '@angular/forms';
// import { MatRadioModule } from '@angular/material/radio';
import { MatSnackBarModule } from '@angular/material/snack-bar';
// import { MatTooltipModule } from '@angular/material/tooltip';
// import { MatButtonModule } from '@angular/material/button';
import { Error404PageComponent } from './404error/404error.component';
import { Error404PageRoutingModule } from './404error-routing.module';

@NgModule({
  declarations: [
    Error404PageComponent
  ],
  imports: [
    CommonModule,
    Error404PageRoutingModule,
    // MatCardModule,
    // MatIconModule,
    // MatFormFieldModule,
    // MatInputModule,
    // ReactiveFormsModule,
    // MatRadioModule,
    MatSnackBarModule,
    // MatTooltipModule,
    // MatButtonModule,
  ]
})
export class Error404PageModule { }
