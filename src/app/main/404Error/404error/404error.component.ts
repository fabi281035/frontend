import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-404error',
  templateUrl: './404error.component.html',
  styleUrls: ['./404error.component.scss']
})
export class Error404PageComponent {
  constructor(
    private snackBar: MatSnackBar,
  ){}
  public profileForm: FormGroup = new FormGroup({
    name: new FormControl('',[Validators.minLength(3)]),
    phone: new FormControl('',[Validators.minLength(12),Validators.maxLength(12)]),
    message : new FormControl(''),
  });

  onSubmit(){
    if(!this.profileForm.valid){
      this.snackBar.open('الرجاء التاكد من المعلومات المدخلة','إلغاء', {
        duration: 3000,
      });
      return
    }
    
    
  }

}
