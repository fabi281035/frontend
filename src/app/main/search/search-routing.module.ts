import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchLayoutComponent } from './search-layout/search-layout.component';


const routes: Routes = [{
  path:'',
  component: SearchLayoutComponent  ,
}];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRoutingModule { }
