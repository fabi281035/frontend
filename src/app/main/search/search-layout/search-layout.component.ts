import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/models/product';

@Component({
  selector: 'app-search-layout',
  templateUrl: './search-layout.component.html',
  styleUrls: ['./search-layout.component.scss']
})
export class SearchLayoutComponent implements OnInit {
  public products: Product[] = []
  products$:Observable<Product[]> | undefined
  private subscription = new Subscription();

  constructor(private route: ActivatedRoute,
    private productService: ProductService,
    ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      let searchKey = params['searchKey'];
      this.products$ = this.productService.getProductsBySearchKey(searchKey || '')
      this.subscription.add(this.products$.subscribe(products => this.products = products))
    });
  }
}
