import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchRoutingModule } from './search-routing.module';
import { SearchLayoutComponent } from './search-layout/search-layout.component';
import { ProductCardModule } from 'src/app/custom-components/product-card/product-card.module';



@NgModule({
  declarations: [
    SearchLayoutComponent
  ],
  imports: [
    CommonModule,
    SearchRoutingModule,
    ProductCardModule
  ]
})
export class SearchModule { }
