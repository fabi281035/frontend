import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CallUsService } from 'src/app/services/call-us.service';
import { PhoneNumberService } from 'src/app/services/phoneNumber.service';

@Component({
  selector: 'app-call-us',
  templateUrl: './call-us.component.html',
  styleUrls: ['./call-us.component.scss']
})
export class CallUsComponent {
  constructor(
    private snackBar: MatSnackBar,
    private callUsServices:CallUsService,
    private phoneNumberService:PhoneNumberService
  ){}
  public profileForm: FormGroup = new FormGroup({
    name: new FormControl('',[Validators.minLength(3)]),
    phoneNumber: new FormControl('',[Validators.minLength(10),Validators.maxLength(15),this.phoneNumberService.phoneValidator(),Validators.pattern(/^\\+963/)]),
    message : new FormControl(''),
  });

  onSubmit(){
    console.log("mmmm",this.profileForm.value)
    if(!this.profileForm.valid){
      this.snackBar.open('الرجاء التاكد من المعلومات المدخلة','إلغاء', {
        duration: 3000,
      });
      return
    }
    this.callUsServices.createMessage(this.profileForm.value)
      .subscribe(
        (response) => {
          console.log('Response from the server:', response);
          this.snackBar.open('لقد تم ارسال رسالتك بنجاح','موافق')
        },
        (error) => {
          console.error('Error:', error);
        }
      );
    
  }

}
