import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CallUsRoutingModule } from './call-us-routing.module';
import { CallUsComponent } from './call-us/call-us.component';
import {MatCardModule} from '@angular/material/card'; 
import {MatIconModule} from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatRadioModule } from '@angular/material/radio';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [
    CallUsComponent
  ],
  imports: [
    CommonModule,
    CallUsRoutingModule,
    MatCardModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatRadioModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatButtonModule,
  ]
})
export class CallUsModule { }
