import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CallUsComponent } from './call-us/call-us.component';

const routes: Routes = [{
  path:'',
  component:CallUsComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CallUsRoutingModule { }
