import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { AffiliateMarketingService } from 'src/app/services/affiliateMarketing.service';
import { AffiliateMarketingCode } from 'src/models/AffiliateMarketing.model';

@Component({
  selector: 'app-marketing-code',
  templateUrl: './marketing-code.component.html',
  styleUrls: ['./marketing-code.component.scss']
})
export class MarketingCodeComponent implements OnInit{

  constructor(private snackBar: MatSnackBar, private affiliateMarketingService:AffiliateMarketingService){}
  code$ = this.affiliateMarketingService.fetchAffiliateMarketing()
  private subscription = new Subscription();
  marketingDetails: AffiliateMarketingCode | undefined

  ngOnInit(): void {
    this.subscription.add(this.code$.subscribe(res => this.marketingDetails = res))
  }

  afterCodeCopied() {
    this.snackBar.open('تم نسخ الكود الى الحافظة', 'حسناً', {
      duration: 3000,
    });
  }
}
