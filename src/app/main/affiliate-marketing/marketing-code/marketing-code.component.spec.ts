import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketingCodeComponent } from './marketing-code.component';

describe('MarketingCodeComponent', () => {
  let component: MarketingCodeComponent;
  let fixture: ComponentFixture<MarketingCodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarketingCodeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MarketingCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
