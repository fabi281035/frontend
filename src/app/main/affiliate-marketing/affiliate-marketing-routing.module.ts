import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AffiliateMarketingLayoutComponent } from './affiliate-marketing-layout/affiliate-marketing-layout.component';
import { MarketingCodeComponent } from './marketing-code/marketing-code.component';

const routes: Routes = [
  {
    path: '',
    component: AffiliateMarketingLayoutComponent
  },
  {
    path: 'code',
    component: MarketingCodeComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AffiliateMarketingRoutingModule { }
