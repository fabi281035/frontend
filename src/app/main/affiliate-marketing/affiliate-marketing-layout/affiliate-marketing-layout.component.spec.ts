import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AffiliateMarketingLayoutComponent } from './affiliate-marketing-layout.component';

describe('AffiliateMarketingLayoutComponent', () => {
  let component: AffiliateMarketingLayoutComponent;
  let fixture: ComponentFixture<AffiliateMarketingLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AffiliateMarketingLayoutComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AffiliateMarketingLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
