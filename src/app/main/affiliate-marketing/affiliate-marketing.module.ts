import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AffiliateMarketingRoutingModule } from './affiliate-marketing-routing.module';
import { AffiliateMarketingLayoutComponent } from './affiliate-marketing-layout/affiliate-marketing-layout.component';
import { MatButtonModule } from '@angular/material/button';
import { MarketingCodeComponent } from './marketing-code/marketing-code.component';
import { MatIconModule } from '@angular/material/icon';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { MatSnackBarModule } from '@angular/material/snack-bar';



@NgModule({
  declarations: [
    AffiliateMarketingLayoutComponent,
    MarketingCodeComponent
  ],
  imports: [
    CommonModule,
    AffiliateMarketingRoutingModule,
    MatButtonModule,
    MatIconModule,
    ClipboardModule,
    MatSnackBarModule
  ]
})
export class AffiliateMarketingModule { }
