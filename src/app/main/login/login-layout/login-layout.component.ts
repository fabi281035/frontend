import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { take, tap } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login-layout',
  templateUrl: './login-layout.component.html',
  styleUrls: ['./login-layout.component.scss']
})
export class LoginLayoutComponent implements OnInit {
  public loginForm: FormGroup = new FormGroup({
    userName: new FormControl(''),
    passWord: new FormControl(''),
  });
  public hide:boolean=false;
  constructor(
    private auth: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {
  }
  
  onSubmit() {
    const { userName, passWord } = this.loginForm.value;
    
    this.auth
      .login( userName, passWord )
      .pipe(
        take(1),
        tap(() => this.router.navigate(['']))
      )
      .subscribe({
        next: () => {
          this.snackBar.open('تم تسجيل الدخول بنجاح!', 'حسناً', {
            duration: 3000,
          });
        },
        error: (err) => {
            this.snackBar.open('البريد الإلكتروني او كلمة المرور غير صحيحة.', 'حسناً', {
              duration: 3000,
            });
            this.loginForm.controls['userName'].setValue('');
        },
      });
  }
}
