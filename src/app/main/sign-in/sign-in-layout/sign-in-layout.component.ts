import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { take, tap } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { LoginForm } from 'src/models/loginForm.model';

@Component({
  selector: 'app-sign-in-layout',
  templateUrl: './sign-in-layout.component.html',
  styleUrls: ['./sign-in-layout.component.scss']
})
export class SignInLayoutComponent {
  
  confirmPasswordValidator: ValidatorFn = (
    control: AbstractControl
  ): { [key: string]: any } | null => {
    return this.isPasswordConfirmed()
      ? { error: 'password not confirmed' }
      : null;
  };

  public signForm: FormGroup = this.formBuilder.group({
    name:new FormControl(''),
    lastName:new FormControl(''),
    email: ['', Validators.email],
    gender:new FormControl(''),
    birthday:['',[
      Validators.required,
      (control: FormControl) => {
        const inputDate = new Date(control.value);
        const currentDate = new Date();
        
        if (!isNaN(inputDate.getTime()) && inputDate <= currentDate) {
          return null; // Valid date and maximum date is current date
        } else {
          return { invalidBirthday: true }; // Invalid date or maximum date exceeded
        }
      }
    ]],
    phone: ['', [Validators.maxLength(12),Validators.pattern(/^\+963/)]],
    password: ['', Validators.minLength(8)],
    confirmPassword:[
      '',
      [Validators.minLength(8), this.confirmPasswordValidator],
    ],
  });
  public hide:boolean=false;
  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  private isPasswordConfirmed(): boolean {
    if (!!this.signForm) {
      const { password, confirmPassword } = this.signForm.value;
      return password === confirmPassword;
    }
    return false;
  }
  ngOnInit(): void {
  }onSubmit() {
    const loginForm:LoginForm = this.signForm.value;
    
    this.auth
      .singIn(loginForm )
      .pipe(
        take(1),
        tap(() => this.router.navigate(['']))
      )
      .subscribe({
        next: () => {
          this.snackBar.open('تم إنشاء حساب جديد بنجاح.!', 'حسناً', {
            duration: 3000,
          });
        },
        error: (err) => {
          console.log({err})
            this.snackBar.open(err.error.message, 'حسناً', {
              duration: 3000,
            });
            this.signForm.controls['userName'].setValue('');
        },
      });
  }
}
