import { formatDate } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { format, lightFormat } from 'date-fns';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { MediaService } from 'src/app/services/media.service';
import { UserService } from 'src/app/services/user.service';
import { userVM } from 'src/models/user.model';

@Component({
  selector: 'profile-in-layout',
  templateUrl: './profile-layout.component.html',
  styleUrls: ['./profile-layout.component.scss']
})
export class ProfileLayoutComponent implements OnInit, OnDestroy{

  constructor(
    private snackBar: MatSnackBar,
    private userService: UserService,private mediaService:MediaService){}

  private subscription = new Subscription();
  
  public profile$ = this.userService.getUserProfile();
  public profileForm: FormGroup = new FormGroup({
    name: new FormControl('',[Validators.minLength(3)]),
    lastName: new FormControl('',[Validators.minLength(3)]),
    birthday: new FormControl('',[]),
    gender: new FormControl('',[]),
    phone: new FormControl('',[Validators.minLength(10),Validators.maxLength(15)]),
    email: new FormControl('',[Validators.email]),
    media: new FormControl(''),
  });

  ngOnInit() {
  this.subscription.add(this.profile$.subscribe(profile=>{
    this.profileForm.setValue({
          name: profile.name,
          lastName: profile.lastName ?? '',
          birthday:  format(new Date (profile.birthday),'yyyy-MM-dd'),
          gender: profile.gender,
          phone: profile.phone,
          email: profile.email,
          media: profile?.profileImage?.media ?? ''
      })
    }))
  }
  onSubmit(){
    if(!this.profileForm.valid){
      this.snackBar.open('الرجاء التاكد من المعلومات المدخلة','إلغاء', {
        duration: 3000,
      });
      return
    }
    this.userService
      .updateUserProfile({...this.profileForm.getRawValue(),mediaId:this.profileForm.get('media')?.value.id})
      .subscribe(
       {
         error:(error)=>{
           this.snackBar.open(error.error.message,'', {
             duration: 3000,
           });
         }
       }
    )
    
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onFileSelected(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    const file = inputElement.files?.[0];
    if(file)
    this.mediaService.postMedia(file)?.subscribe(media =>{
      this.profileForm.get('media')?.setValue(media)
    })
  }

}
