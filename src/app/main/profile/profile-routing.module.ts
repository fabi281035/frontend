import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileLayoutComponent } from './profile-layout/profile-layout.component';
import { AuthGuard } from 'src/app/guards/logIn.guard';

const routes: Routes = [{
  path:'',
  canActivate:[AuthGuard],
  component:ProfileLayoutComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
