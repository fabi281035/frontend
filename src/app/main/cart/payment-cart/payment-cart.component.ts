import { Component, OnInit } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from 'src/app/services/auth.service';
import { ProductCartService } from 'src/app/services/productCart.service';
import { ProductCartLocalStorageService } from 'src/app/services/productCartLocalStorage.service';
import { UserService } from 'src/app/services/user.service';
import { ProductCart } from 'src/models/productCart';

export interface UserAddress {
  id:number,
  name: string,
  description:string
  city: string
  country: string
  deliveryCost: number
}

interface PaymentParams {
  productsTotalPrice: number,
  userAddress?: UserAddress[],
  deliveringCost: number,
  finialPrice:number,
  products: ProductCart[],
  currentAddress?: UserAddress,
  paymentDone: boolean
}
@Component({
  selector: 'app-payment-cart',
  templateUrl: './payment-cart.component.html',
  styleUrls: ['./payment-cart.component.scss']
})
export class PaymentCartComponent implements OnInit {

  params:PaymentParams = {
    deliveringCost: 0,
    finialPrice: 0,
    productsTotalPrice: 0,
    products: [],
    paymentDone: false
  }

  constructor(private productCartService: ProductCartService, 
    private userService:UserService, private auth:AuthService,
    private productCartLocalStorageService:ProductCartLocalStorageService,
    private snackBar:MatSnackBar ){}
  
  ngOnInit() {
    this.userService.getUserAddress().subscribe(res =>{
      this.params.userAddress = res      
    })

    this.params.products = this.productCartLocalStorageService.getCartFromLocalStorage() as ProductCart[]
    this.params.productsTotalPrice =  this.params.products.reduce((currentSum,product) => currentSum + product.totalPrice!, 0)
    this.params.deliveringCost = 0
    this.params.finialPrice = this.params.deliveringCost + this.params.productsTotalPrice
  }

  async createNewCart() {
    if(!this.params.currentAddress){ 
        this.snackBar.open('الرجاء إدخال مكان التوصيل','إلغاء', {
          duration: 3000,
        });
        return
      }    
      if(this.params.products.length <=  0){ 
        this.snackBar.open('لا يوجد منتجات الرجاء ملئ السلة','إلغاء', {
          duration: 3000,
        });
        return
      }    
      this.productCartService.createCart({
        products: this.getProducts(),
        addressId: this.params.currentAddress?.id!
      }).subscribe(()=>{
          this.params.paymentDone = true
          this.productCartLocalStorageService.removeProductsFromCart()
      })
  }

  getProducts() {
    return this.params.products.map(product => {return { productId:product.id,size:product.size, color: product.color, quantity:product.quantity, price:product.price}})
  }

  currentAddress(event: MatSelectChange) {
    this.params.currentAddress = event.value
    this.params.finialPrice = (this.params.currentAddress?.deliveryCost || 0) + this.params.productsTotalPrice  
  }

}
