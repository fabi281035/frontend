import { Component, Input } from '@angular/core';
import { ProductCartLocalStorageService } from 'src/app/services/productCartLocalStorage.service';
import { ProductCart, defaultProductCart, getDefaultProductCart } from 'src/models/productCart';

@Component({
  selector: 'app-product-cart',
  templateUrl: './product-cart.component.html',
  styleUrls: ['./product-cart.component.scss']
})
export class ProductCartComponent {
  @Input() productCart :ProductCart = getDefaultProductCart()
  productDeleted = false

  constructor(private productCartService:ProductCartLocalStorageService){}
  
  increaseProductQuantity() {
    this.productCart.quantity += 1 
    this.calculateTotalPrice()
    this.saveCurrentProductInLocalStorage()
  }

  decreaseProductQuantity() {
    this.productCart.quantity -= 1 
    this.calculateTotalPrice()
    this.saveCurrentProductInLocalStorage()
  }

  deleteProductFromCart() {
    this.productCartService.removeProductToCart(this.productCart)
    this.productDeleted = true
  }

  calculateTotalPrice() {
    this.productCart.totalPrice = this.productCart.price * this.productCart.quantity
  }

  saveCurrentProductInLocalStorage(){
    this.productCartService.updateProductInCurrentCart(this.productCart)
  }
}
