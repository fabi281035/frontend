import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartLayoutComponent } from './cart-layout/cart-layout.component';
import { CartRoutingModule } from './cart-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ProductCartComponent } from './product-cart/product-cart.component';
import { PaymentCartComponent } from './payment-cart/payment-cart.component';
import { MatDividerModule } from '@angular/material/divider';
import {MatSelectModule} from '@angular/material/select';
import { CartListComponent } from './cart-list/cart-list.component';
import { CartInListComponent } from './cart-list/cart-in-list/cart-in-list.component';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';



@NgModule({
  declarations: [
    CartLayoutComponent,
    ProductCartComponent,
    PaymentCartComponent,
    CartListComponent,
    CartInListComponent
  ],
  imports: [
    CommonModule,
    CartRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatSelectModule,
    MatSnackBarModule
  ]
})
export class CartModule { }
