import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ProductCartService } from 'src/app/services/productCart.service';
import { CartList } from 'src/models/cart';

@Component({
  selector: 'app-cart-list',
  templateUrl: './cart-list.component.html',
  styleUrls: ['./cart-list.component.scss']
})
export class CartListComponent implements OnInit{

  constructor(private cartService: ProductCartService){}
  cartsList: CartList[] = []

  private subscription = new Subscription()
  cartsList$ = this.cartService.getAllCarts();

  ngOnInit(){
    this.subscription.add(this.cartsList$.subscribe((res)=>{
      this.cartsList = res
      console.log(this.cartsList);
    }))
  }

}
