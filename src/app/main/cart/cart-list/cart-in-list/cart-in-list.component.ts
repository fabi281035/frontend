import { Component, OnInit,Input } from '@angular/core';
import { ProductCartService } from 'src/app/services/productCart.service';
import { ProductCart } from 'src/models/productCart';
import { Subscription } from 'rxjs';
import { CartList, CartStatus } from 'src/models/cart';

export enum CartStatusShowing {
  INPROGRESS = 'جار التحضير',
  REJECTED = 'مرفوضة',
  ONWAY = 'في الطريق' ,
  DELIVERED = 'تم التوصيل',
  undefined = 'لا يوجد حالة'
}


@Component({
  selector: 'app-cart-in-list',
  templateUrl: './cart-in-list.component.html',
  styleUrls: ['./cart-in-list.component.scss']
})
export class CartInListComponent implements OnInit {


  @Input() shortCart: CartList | undefined;

  
  async ngOnInit() {
    
  }

  getFullCost(){
    return (this.shortCart?.totalPrice || 0) + (this.shortCart?.deliveryCost || 0)
  }

  getStatus() {
    return  CartStatusShowing[ (this.shortCart?.status != undefined) ? this.shortCart?.status : 'undefined']
  }
}
