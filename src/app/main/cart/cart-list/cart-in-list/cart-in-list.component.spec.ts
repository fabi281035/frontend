import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CartInListComponent } from './cart-in-list.component';

describe('CartInListComponent', () => {
  let component: CartInListComponent;
  let fixture: ComponentFixture<CartInListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CartInListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CartInListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
