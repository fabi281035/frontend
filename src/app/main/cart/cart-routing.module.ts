import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartLayoutComponent } from './cart-layout/cart-layout.component';
import { PaymentCartComponent } from './payment-cart/payment-cart.component';
import { CartListComponent } from './cart-list/cart-list.component';
import { CartInListComponent } from './cart-list/cart-in-list/cart-in-list.component';
import { AuthGuard } from 'src/app/guards/logIn.guard';


const routes: Routes = [
  {
    path:'',  
    canActivate:[AuthGuard],
    component:CartLayoutComponent
  },
  {
    path:'payment',
    component: PaymentCartComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'list',
    component: CartListComponent
  }
];

@NgModule({
  imports: [RouterModule .forChild(routes)],
  exports: [RouterModule]

})
export class CartRoutingModule { }
