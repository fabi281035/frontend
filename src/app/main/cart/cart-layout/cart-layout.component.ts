import { Component, OnInit } from '@angular/core';
import { ProductCartService } from 'src/app/services/productCart.service';
import { ProductCartLocalStorageService } from 'src/app/services/productCartLocalStorage.service';
import { ProductCart } from 'src/models/productCart';

@Component({
  selector: 'app-cart-layout',
  templateUrl: './cart-layout.component.html',
  styleUrls: ['./cart-layout.component.scss']
})
export class CartLayoutComponent implements OnInit{

  constructor(private  productCartService:ProductCartService,private productCartLocalStorageService:ProductCartLocalStorageService){}
  currentCart: ProductCart[] = this.productCartLocalStorageService.getCartFromLocalStorage()
  isDisabled: boolean = false
  ngOnInit(){
    this.currentCart = this.productCartLocalStorageService.getCartFromLocalStorage()
    this.isDisabled = this.currentCart.length > 0
  }

}
