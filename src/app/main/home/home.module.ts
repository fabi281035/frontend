import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeLayoutComponent } from './home-layout/home-layout.component';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { HomeRoutingModule } from './home-routing.module';
import { HomeMainSliderComponent } from './home-main-slider/home-main-slider.component';
import { ProductCardModule } from 'src/app/custom-components/product-card/product-card.module';
import { ProductSliderModule } from 'src/app/custom-components/product-slider/product-slider.module';
import { SpecialOfferModule } from 'src/app/custom-components/special-offer/special-offer.module';
import { CategoryCardsModule } from 'src/app/custom-components/category-cards/category-cards.module';


@NgModule({
  declarations: [
    HomeLayoutComponent,
    HomeMainSliderComponent
  ],
  imports: [ 
    CommonModule,
    HomeRoutingModule,
    MatButtonModule,
    MatIconModule,
    ProductCardModule,
    ProductSliderModule,
    SpecialOfferModule,
    CategoryCardsModule,
  ],
})
export class HomeModule { }
