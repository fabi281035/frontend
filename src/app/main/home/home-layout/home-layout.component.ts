import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ProductOfferService } from 'src/app/services/prodict-offer.service';
import { ProductService } from 'src/app/services/product.service';
import { OfferWithProducts } from 'src/models/offer';
import { Product } from 'src/models/product';

@Component({
  selector: 'app-home-layout',
  templateUrl: './home-layout.component.html',
  styleUrls: ['./home-layout.component.scss']
})
export class HomeLayoutComponent implements OnInit, OnDestroy {
  private products$ = this.productService.getProducts();
  private newProduct$ = this.productService.getNewProducts();
  private offers$ = this.productOfferService.getNewestOffer();
  private mostSellingProduct$ = this.productService.getMostSellingProduct();
  public newProducts:Product[]=[];
  public products:Product[]=[];
  public mostSellingProducts:Product[]=[];
  public offers: OfferWithProducts[] = [];

  private subscription = new Subscription()

  constructor(private productService: ProductService,private productOfferService:ProductOfferService) { }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  ngOnInit(): void {

    this.subscription.add(this.newProduct$.subscribe((products)=>this.newProducts=products));
    this.subscription.add(this.products$.subscribe((products)=>this.products=products));
    this.subscription.add(this.mostSellingProduct$.subscribe((products)=>this.mostSellingProducts = products));
    this.subscription.add(this.offers$.subscribe((offers)=> this.offers = offers));
  }

}
