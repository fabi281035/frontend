import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WomenLayoutComponent } from './women-layout/women-layout.component';

const routes: Routes = [{
  path:'',
  component:WomenLayoutComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WomenRoutingModule { }
