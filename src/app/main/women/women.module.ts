import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WomenRoutingModule } from './women-routing.module';
import { WomenLayoutComponent } from './women-layout/women-layout.component';
import { ProductCardModule } from 'src/app/custom-components/product-card/product-card.module';


@NgModule({
  declarations: [
    WomenLayoutComponent
  ],
  imports: [
    CommonModule,
    WomenRoutingModule,
    ProductCardModule,
  ]
})
export class WomenModule { }
