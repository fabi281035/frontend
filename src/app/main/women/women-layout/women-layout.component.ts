import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-women-layout',
  templateUrl: './women-layout.component.html',
  styleUrls: ['./women-layout.component.scss']
})
export class WomenLayoutComponent  {
  public products$ = this.productService.getProductsByCategoryId('2');

  constructor(private productService: ProductService) { }
}