import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandsLayoutComponent } from './brands-layout.component';

describe('BrandsLayoutComponent', () => {
  let component: BrandsLayoutComponent;
  let fixture: ComponentFixture<BrandsLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrandsLayoutComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BrandsLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
