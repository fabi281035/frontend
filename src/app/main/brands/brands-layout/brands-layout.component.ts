import { Component } from '@angular/core';
import { BrandService } from 'src/app/services/brand.service';

@Component({
  selector: 'app-brands-layout',
  templateUrl: './brands-layout.component.html',
  styleUrls: ['./brands-layout.component.scss']
})
export class BrandsLayoutComponent {
  public brands$ = this.brandService.getBrands();

  constructor(private brandService:BrandService){}

}
