import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrandsRoutingModule } from './brands-routing.module';
import { BrandsLayoutComponent } from './brands-layout/brands-layout.component';
import { BrandCardComponent } from './brand-card/brand-card.component';
import { BrandProductsLayoutComponent } from './brand-products-layout/brand-products-layout.component';
import { ProductCardModule } from 'src/app/custom-components/product-card/product-card.module';
import { ProductSliderModule } from 'src/app/custom-components/product-slider/product-slider.module';


@NgModule({
  declarations: [
    BrandsLayoutComponent,
    BrandCardComponent,
    BrandProductsLayoutComponent
  ],
  imports: [
    CommonModule,
    BrandsRoutingModule,
    ProductCardModule,
    ProductSliderModule
  ]
})
export class BrandsModule { }
