import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandProductsLayoutComponent } from './brand-products-layout.component';

describe('BrandProductsLayoutComponent', () => {
  let component: BrandProductsLayoutComponent;
  let fixture: ComponentFixture<BrandProductsLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrandProductsLayoutComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BrandProductsLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
