import { Product } from 'src/models/product';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
import { BrandVM } from 'src/models/brand';
import { BrandService } from 'src/app/services/brand.service';

@Component({
  selector: 'app-brand-products-layout',
  templateUrl: './brand-products-layout.component.html',
  styleUrls: ['./brand-products-layout.component.scss']
})
export class BrandProductsLayoutComponent implements OnInit, OnDestroy {
  private brandId:string='';
  
  public brand: BrandVM | undefined 
  public offerProducts : Product[] = [];

  private subscription = new Subscription();


  constructor(private route: ActivatedRoute, private productService: ProductService, private brandService:BrandService) { }
  
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.brandId = params['id'];
      const products$ = this.productService.getProductsByBrandId(this.brandId);
      this.subscription.add(products$.subscribe( (brand)=>
        {
          this.brand = brand; 
          this.offerProducts = this.brand?.offers[0]?.products || [] 
        } ))
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  showOffers() {
    return this.brand?.offers.length || -1 > 0 
  }

  getExpiryOfferDate(){
    return this.brand?.offers[0].expiryDate?.toString() || ''
  }

}
