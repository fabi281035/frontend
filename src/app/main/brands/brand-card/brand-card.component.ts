import { Component, Input } from '@angular/core';
import { Brand } from 'src/models/product';

@Component({
  selector: 'app-brand-card',
  templateUrl: './brand-card.component.html',
  styleUrls: ['./brand-card.component.scss']
})
export class BrandCardComponent {

  @Input() brand:Brand|undefined

}
