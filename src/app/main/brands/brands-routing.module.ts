import { BrandProductsLayoutComponent } from './brand-products-layout/brand-products-layout.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrandsLayoutComponent } from './brands-layout/brands-layout.component';
const routes: Routes = [
  {
    path:'',
    component:BrandsLayoutComponent
  },
  {
    path:':id',
    component:BrandProductsLayoutComponent,
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BrandsRoutingModule { }
