import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { ProductCartLocalStorageService } from 'src/app/services/productCartLocalStorage.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  searchKey: string = '';
  private subscription = new Subscription();
  
  public profile$ = this.userService.getUserProfile();
  userProfileImageUrl=''
  constructor(
    private router: Router,
    private route: ActivatedRoute, 
    private productCartService:ProductCartLocalStorageService, 
    private matIconRegistry: MatIconRegistry, 
    private domSanitizer: DomSanitizer,
    public auth:AuthService,
    private userService: UserService
  ) {
    this.matIconRegistry.addSvgIcon('cart-icon', this.domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/cart-icon.svg'));
  }
  ngOnInit(): void {
    this.subscription.add(this.profile$.subscribe(data=>{
      this.userProfileImageUrl=data.profileImage.media.url;
    }))
  }
  
  
  currentUser = this.auth.currentUser.getValue()
  logOut(){
  this.route.queryParams.subscribe(params => {
    this.searchKey = params['searchKey'];
    console.log({sa:this.searchKey});
    
  })
 
  this.auth.logout();
  this.auth.checkUser
  }
  haveProductInCart(){
    return this.productCartService.getCartFromLocalStorage().length  > 0
  }

  search() {
    this.router.navigate(['/search'], { queryParams: { searchKey: this.searchKey } });
  }
}
