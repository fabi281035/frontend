import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductFavoriteLayoutComponent } from './product-favorite-layout/product-favorite-layout.component';

const routes: Routes = [
  {
    path: '',
    component: ProductFavoriteLayoutComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductFavoriteRoutingModule { }
