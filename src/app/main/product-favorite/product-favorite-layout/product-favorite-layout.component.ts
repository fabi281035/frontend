import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, Subscription, distinctUntilChanged } from 'rxjs';
import { ProductService } from 'src/app/services/product.service';
import { Product, productFavoredChangeStatus } from 'src/models/product';

@Component({
  selector: 'app-product-favorite-layout',
  templateUrl: './product-favorite-layout.component.html',
  styleUrls: ['./product-favorite-layout.component.scss']
})
export class ProductFavoriteLayoutComponent implements OnInit {

  products$:Observable<Product[]> | undefined
  public products = new BehaviorSubject<Product[]>([])
  private subscription = new Subscription();

  constructor(private productService:ProductService){}
  ngOnInit(): void {
    this.products$ = this.productService.getProductsFavorite()
    this.subscription.add(this.products$?.subscribe(products => this.products.next(products)))
    this.subscription.add(this.productService.productFavoredChange.subscribe(item=>{
      item?this.updateProductList(item?.product,item?.changeStatus):null
    }))
  }

  updateProductList(product:Product,changeStatus:productFavoredChangeStatus){
    if(changeStatus=='add'){
      const newProductsArray=[...this.products.value,product]
      this.products.next(newProductsArray)
    }else if( changeStatus =='delete'){
      const newProductsArray=[...this.products.value.filter(item=>item.id!=product.id)]
      this.products.next(newProductsArray);
    }
  }
}
