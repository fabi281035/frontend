import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ProductFavoriteRoutingModule } from './product-favorite-routing.module';
import { ProductFavoriteLayoutComponent } from './product-favorite-layout/product-favorite-layout.component';
import { MatIconModule } from '@angular/material/icon';
import { ProductCardModule } from 'src/app/custom-components/product-card/product-card.module';



@NgModule({
  declarations: [
    ProductFavoriteLayoutComponent
  ],
  imports: [
    CommonModule,
    ProductFavoriteRoutingModule,
    MatIconModule,
    ProductCardModule
  ]
})
export class ProductFavoriteModule { }
