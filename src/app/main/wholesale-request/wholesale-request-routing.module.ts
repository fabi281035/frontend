import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WholesaleRequestLayoutComponent } from './wholasele-request-layout/wholesale-request-layout.component';


const routes: Routes = [{
  path:'',
  component: WholesaleRequestLayoutComponent,
}];


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class WholesaleRequestRoutingModule { }
