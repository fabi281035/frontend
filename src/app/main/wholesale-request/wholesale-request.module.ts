import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WholesaleRequestLayoutComponent } from './wholasele-request-layout/wholesale-request-layout.component';
import { WholesaleRequestRoutingModule } from './wholesale-request-routing.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';



@NgModule({
  declarations: [
    WholesaleRequestLayoutComponent
  ],
  imports: [
    CommonModule,
    WholesaleRequestRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatSnackBarModule,
    MatIconModule,
    FormsModule,
    MatSelectModule,
    MatRadioModule
  ]
})
export class WholesaleRequestModule { }
