import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { WholesaleRequestService } from 'src/app/services/wholesaleRequest.service';

@Component({
  selector: 'app-wholesale-request-layout',
  templateUrl: './wholesale-request-layout.component.html',
  styleUrls: ['./wholesale-request-layout.component.scss']
})
export class WholesaleRequestLayoutComponent {

  public wholesaleRequestForm: FormGroup = new FormGroup({
    brandName: new FormControl('', [Validators.required]),
    brandNameOwnerName: new FormControl('', [Validators.required]),
    address: new FormControl('', [Validators.required]),
    phoneNumber: new FormControl('', [Validators.required, Validators.minLength(13), Validators.maxLength(13)]),
    branchesNumber: new FormControl('', [Validators.required]),
    type: new FormControl('', [Validators.required]),
  });

  constructor(private snackBar: MatSnackBar, private wholesaleRequestService:WholesaleRequestService){}
  
  addNewWholesaleRequest() {
    if(this.wholesaleRequestForm.invalid) {
      this.snackBar.open('الرجاء ادخال كافة المعلومات','حسناً', {
        duration: 3000,
      });
      return
    }
    this.wholesaleRequestService.createNewWholesaleRequest(this.wholesaleRequestForm.value)
    this.wholesaleRequestForm.reset()
    this.snackBar.open('تم اضافة طلبك بنجاح','حسناً', {
      duration: 3000,
    });
    
  }

}
