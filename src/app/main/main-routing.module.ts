import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeLayoutComponent } from './home/home-layout/home-layout.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { CartLayoutComponent } from './cart/cart-layout/cart-layout.component';
import { AuthGuard } from '../guards/logIn.guard';

const routes: Routes = [
  {
    path: 'women',
    component: MainLayoutComponent,
    loadChildren: () => import('./women/women.module').then((m) => m.WomenModule),
  },
  {
    path: 'men',
    component: MainLayoutComponent,
    loadChildren: () => import('./men/men.module').then((m) => m.MenModule),
  },
  {
    path: 'product',
    component: MainLayoutComponent,
    loadChildren: () => import('./products/products.module').then((m) => m.ProductsModule),
  },
  {
    path: 'kids',
    component: MainLayoutComponent,
    loadChildren: () => import('./kids/kids.module').then((m) => m.KidsModule),
  },
  {
    path: 'join-us',
    component: MainLayoutComponent,
    loadChildren: () => import('./join-us/join-us.module').then((m) => m.JoinUsModule),
  },
  {
    path: 'call-us',
    component: MainLayoutComponent,
    loadChildren: () => import('./call-us/call-us.module').then((m) => m.CallUsModule),
  },
  {
    path: 'error-page',
    component: MainLayoutComponent,
    loadChildren: () => import('./404Error/Error404Page.module').then((m) => m.Error404PageModule),
  },
  {
    path: 'brands',
    component: MainLayoutComponent,
    loadChildren: () => import('./brands/brands.module').then((m) => m.BrandsModule),
  },
  {
    path: 'login',
    component: MainLayoutComponent,
    loadChildren: () => import('./login/login.module').then((m) => m.LoginModule),
  },
  {
    path: 'sign-in',
    component: MainLayoutComponent,
    loadChildren: () => import('./sign-in/sign-in.module').then((m) => m.SignInModule),
  },
  {
    path: '',
    component: MainLayoutComponent,
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
  },
  
  {
    path: 'profile',
    component: MainLayoutComponent,
    loadChildren: () => import('./profile/profile.module').then((m) => m.ProfileModule),
  },

  {
    path: 'wholesale',
    component: MainLayoutComponent,
    loadChildren: () => import('./wholesale-request/wholesale-request.module').then((m) => m.WholesaleRequestModule),
  },

  { 
    path: 'cart',
    component: MainLayoutComponent,
    loadChildren: () => import('./cart/cart.module').then((m) => m.CartModule),
  },
  { 
    path: 'search',
    component: MainLayoutComponent,
    loadChildren: () => import('./search/search.module').then((m) => m.SearchModule),
  },
  { 
    path: 'address',
    component: MainLayoutComponent,
    loadChildren: () => import('./address/address.module').then((m) => m.AddressModule),
    canActivate: [AuthGuard]
  },
  { 
    path: 'affiliate-marketing',
    component: MainLayoutComponent,
    loadChildren: () => import('./affiliate-marketing/affiliate-marketing.module').then((m) => m.AffiliateMarketingModule),
    canActivate: [AuthGuard]
  },
  { 
    path: 'product-favorite',
    component: MainLayoutComponent,
    loadChildren: () => import('./product-favorite/product-favorite.module').then((m) => m.ProductFavoriteModule),
    canActivate: [AuthGuard]
  },
  
  { path: '', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
