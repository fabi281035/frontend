import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JoinUsComponent } from './join-us/join-us.component';
import { AuthGuard } from 'src/app/guards/logIn.guard';

const routes: Routes = [{
  path:'',
  canActivate:[AuthGuard],
  component:JoinUsComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JoinUsRoutingModule { }
