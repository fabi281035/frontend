import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { JoinUsService } from 'src/app/services/join-us.service';
import { PhoneNumberService } from 'src/app/services/phoneNumber.service';

@Component({
  selector: 'app-join-us',
  templateUrl: './join-us.component.html',
  styleUrls: ['./join-us.component.scss']
})
export class JoinUsComponent {

  constructor(
    private snackBar: MatSnackBar,
    private joinUseService:JoinUsService,
    private route :Router,
    private phoneNumberService: PhoneNumberService
  ){

  }
  joinUs ='انضم الينا كبائع او مشتري لتستفيد من عروض الجملة(شراء/ بيع) يرجى  تعبئة الاستمارة التالية'
  public profileForm: FormGroup = new FormGroup({
    brandName: new FormControl('',[Validators.minLength(3)]),
    brandOwnerName: new FormControl('',[Validators.minLength(3)]),
    phone: new FormControl('',[Validators.minLength(10),Validators.maxLength(15),this.phoneNumberService.phoneValidator()]),
    branchNumber: new FormControl(1,[Validators.min(1)]),
    address: new FormControl(''),
  });
  onSubmit(){
    if(!this.profileForm.valid){
      this.snackBar.open('الرجاء التاكد من المعلومات المدخلة','إلغاء', {
        duration: 3000,
      });
      return
    }
    this.joinUseService.joinUsSubmit(this.profileForm.value).subscribe(
      (response)=>{
        console.log("get response from join us backend:",response);
        this.snackBar.open('لقد تم ارسال طلبك بنجاح','موافق',{
          duration: 3000
        });
        this.route.navigate([''])
      },
      (error) => {
        console.log("there is error submitting join us form",error)
        this.snackBar.open('هناك خطا ما تاكد من المعلومات المدخلة','موافق',{
          duration:3000
        })        
      }
    )
  }
}
