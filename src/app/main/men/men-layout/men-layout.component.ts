import { Component } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-men-layout',
  templateUrl: './men-layout.component.html',
  styleUrls: ['./men-layout.component.scss']
})
export class MenLayoutComponent {
  public products$ = this.productService.getProductsByCategoryId('1');

  constructor(private productService: ProductService) { }
}