import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenRoutingModule } from './men-routing.module';
import { MenLayoutComponent } from './men-layout/men-layout.component';
import { ProductCardModule } from 'src/app/custom-components/product-card/product-card.module';


@NgModule({
  declarations: [
    MenLayoutComponent
  ],
  imports: [
    CommonModule,
    MenRoutingModule,
    ProductCardModule,
  ]
})
export class MenModule { }
