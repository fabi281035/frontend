import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Review } from 'src/models/product';

@Component({
  selector: 'app-rate',
  templateUrl: './rate.component.html',
  styleUrls: ['./rate.component.scss']
})
export class RateComponent {

  @Input() review: Review | undefined
  @Output() starSelected: EventEmitter<number> = new EventEmitter<number>(); // Event emitter for star selection

  stars: number[] = [1, 2, 3, 4, 5]; // Array representing the stars

}
