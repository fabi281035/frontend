import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Route } from '@angular/router';
import { Subscription } from 'rxjs';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/models/product';

@Component({
  selector: 'app-product-rating',
  templateUrl: './product-rating.component.html',
  styleUrls: ['./product-rating.component.scss']
})
export class ProductRatingComponent implements OnInit {

  product: Product | undefined
  productId: string = ''
  constructor(private route: ActivatedRoute,private productService:ProductService){}

  private subscription = new Subscription();

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.productId = params['id'];
      const product$ = this.productService.getProductsById(this.productId);
      this.subscription.add(product$.subscribe((product)=>{
          this.product = product
          console.log(this.product);
          
        }))
    });
  }

}
