import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductDetailComponent } from './product-detail.component';
import { ProductRatingComponent } from './product-rating/product-rating.component';
import {MatTabsModule} from '@angular/material/tabs';
import { RateComponent } from './product-rating/rate/rate.component';
import { MatDividerModule } from '@angular/material/divider';



@NgModule({
  declarations: [
    ProductDetailComponent,
    ProductRatingComponent,
    RateComponent
  ],
  imports: [
    CommonModule,
    MatTabsModule,
    MatDividerModule
  ],
  exports:[ProductDetailComponent]
})
export class ProductDetailModule { }
