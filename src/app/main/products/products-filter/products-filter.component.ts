import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { CategoriesService } from 'src/app/services/categories.service';
import { ProductService } from 'src/app/services/product.service';
import { CategoryWithCountProductInBrand } from 'src/models/category';
import { Product } from 'src/models/product';

@Component({
  selector: 'app-products-filter',
  templateUrl: './products-filter.component.html',
  styleUrls: ['./products-filter.component.scss']
})
export class ProductsFilterComponent implements OnInit {

  public categoryId: string = '';
  public brandId: string = '';
  public products:Product[] | undefined =[]
  public categoryWithProductCount: CategoryWithCountProductInBrand | undefined
  private subscription = new Subscription();
  products$:Observable<Product[]> | undefined
  category$:Observable<CategoryWithCountProductInBrand> | undefined

  constructor( private route: ActivatedRoute,
    private productService: ProductService,
      private categoryService: CategoriesService){
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.categoryId = params['categoryId'];
      this.brandId = params['brandId'];
      this.products$ = this.productService.getProductsByCategoryIdAndBrandId(this.categoryId,this.brandId)
      this.category$ = this.categoryService.getCategoryWithProductCountInBrand(this.categoryId,this.brandId)
      this.subscription.add(this.products$.subscribe(product=> this.products = product))
      this.subscription.add(this.category$.subscribe(category=> {this.categoryWithProductCount = category; console.log(this.categoryWithProductCount);
      }))
      
    })
  }


}
