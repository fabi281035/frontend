import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsRoutingModule } from './products-routing.module';
import { ProductLayoutComponent } from './product-layout/product-layout.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ProductsFilterComponent } from './products-filter/products-filter.component';
import { ProductCardModule } from 'src/app/custom-components/product-card/product-card.module';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductRatingComponent } from './product-detail/product-rating/product-rating.component';
import { ProductDetailModule } from './product-detail/product-detail.module';
import { ProductSliderModule } from 'src/app/custom-components/product-slider/product-slider.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ProductLayoutComponent,
    ProductsFilterComponent,

  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatSnackBarModule,
    ProductCardModule,
    ProductDetailModule,
    ProductSliderModule,
    ReactiveFormsModule
  ]
})
export class ProductsModule { }
