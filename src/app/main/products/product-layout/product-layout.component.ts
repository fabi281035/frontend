import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { ProductService } from 'src/app/services/product.service';
import { ProductCartLocalStorageService } from 'src/app/services/productCartLocalStorage.service';
import { Product, ProductDetail } from 'src/models/product';

@Component({
  selector: 'app-product-layout',
  templateUrl: './product-layout.component.html',
  styleUrls: ['./product-layout.component.scss']
})
export class ProductLayoutComponent implements OnInit, OnDestroy {
  private productId:string='';
  public product : Product|undefined;  
  private mostSellingProduct$ = this.productService.getMostSellingProduct();
  public mostSellingProducts:Product[]=[];
  public currentQuantity = 1;
  public currentProductImageUrl = ''
  public currentProductSize = 'XL'
  public currentProductDetailSelected: ProductDetail | undefined
  public sizes =[
    {label:'S',  isActive:false, value: 'S' , isSelected: false},
    {label:'M',  isActive:false, value: 'M' , isSelected: false},
    {label:'L',  isActive:false, value: 'L' , isSelected: false},
    {label:'XL', isActive:false, value: 'XL' , isSelected: false},
    {label:'XXL',isActive:false, value: 'XXL' , isSelected: false},
    {label:'3XL',isActive:false, value: 'XXXL' , isSelected: false},
    {label:'4XL',isActive:false, value: 'XXXXL' , isSelected: false},
    {label:'5XL',isActive:false, value: 'XXXXXL' , isSelected: false},
  ];
  

  private subscription = new Subscription();


  constructor( 
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private productService: ProductService,
    private productCartService:ProductCartLocalStorageService,
    private authService: AuthService,
    private router: Router
    ) { }
  
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.productId = params['id'];
      console.log('ID:', this.productId );
      const product$ = this.productService.getProductsById(this.productId);
      this.subscription.add(this.mostSellingProduct$.subscribe((products)=>this.mostSellingProducts = products));
      this.subscription.add(product$.subscribe((product)=>{
          this.product = product
          this.checkAvailableSize()
          this.currentProductImageUrl = product?.mainPhoto?.url
        }))
    });
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  checkAvailableSize(){
    for (const size of this.sizes) {
      size.isActive = this.product?.productDetails?.some(detail => detail.size.includes(size.value)) || false;
    }
  }

  
  addToFavorite() {
    if (this.product) {
      this.product.isFavorite = !this.product.isFavorite;
    } 
  }

  handleChangesByColor(detail: ProductDetail){
    if(detail.media)
      this.currentProductImageUrl = detail.media.url
    else
      this.currentProductImageUrl = this.product?.mainPhoto.url || ''

    this.currentProductDetailSelected = detail

  }

  selectSize(sizeToSelect: {label:string,isSelected:boolean,isActive:boolean,value:string}){
    this.sizes.map(size => size.label != sizeToSelect.label ? size.isSelected = false : size.isSelected = true )
    this.currentProductSize = sizeToSelect.value
  }

  addProductToCart(){
    this.productCartService.addProductToCart({
      color: this.currentProductDetailSelected?.color || '',
      id: this.product?.id!,
      imageUrl: this.currentProductDetailSelected?.media.url!,
      name: this.product?.name! ||  '',
      price: this.product?.price!,
      quantity: this.currentQuantity,
      size: this.currentProductSize,
      totalPrice: this.product?.price! * this.currentQuantity
    }) 
    this.snackBar.open('تم إضافة المنتج الى السلة', 'حسناً', {
      duration: 3000,
    });
    
  }
  decreaseProductQuantity(){
    if(this.currentQuantity > 1)
      this.currentQuantity --;
  }
  increaseProductQuantity(){
    this.currentQuantity ++;
  }


  // add new Rate
  public rateForm: FormGroup = new FormGroup({
    rateDescription: new FormControl(''),
  });
  selectedStar: number = 0; // The selected star (initially set to 0)

  starSelected: EventEmitter<number> = new EventEmitter<number>(); // Event emitter for star selection

  stars: number[] = [1, 2, 3, 4, 5]; // Array representing the stars

  onStarClick(selected: number): void {
    this.selectedStar = selected; // Set the selected star
    this.starSelected.emit(selected); // Emit the selected star event
  }

  addNewRate(){
    const currentUserId=this.authService.currentUser.value?.id!
    if(!currentUserId){
      this.snackBar.open('يجب عليك تسجيل الدخول اولا ','موافق');
      this.router.navigate(['/login']);
    }
    this.productService.postAddReview({
      userId: currentUserId,
      rate: this.selectedStar,
      description: this.rateForm.get('rateDescription')?.value,
      productId: this.product?.id!
    }).subscribe( _ =>{
        this.rateForm.setValue({
          rateDescription: '',
          
        })
        this.selectedStar = 0
    })
    
  }
}
