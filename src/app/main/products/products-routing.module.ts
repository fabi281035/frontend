import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductLayoutComponent } from './product-layout/product-layout.component';
import { ProductsFilterComponent } from './products-filter/products-filter.component';

const routes: Routes = [{
    path:':id',
    component:ProductLayoutComponent,
  },
  {
    path:'category/:categoryId/brand/:brandId',
    component: ProductsFilterComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
