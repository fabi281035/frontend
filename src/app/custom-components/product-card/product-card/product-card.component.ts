import { Component, Input, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProductService } from 'src/app/services/product.service';
import { ProductCartLocalStorageService } from 'src/app/services/productCartLocalStorage.service';
import { Product, productFavoredChangeStatus } from 'src/models/product';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit{
  @Input() product: Product|undefined;

  public favorite: boolean = false;

  constructor(private productCartService: ProductCartLocalStorageService,private snackBar:MatSnackBar,private productService:ProductService){}
  ngOnInit(): void {
    if(this.product){
      this.productService.getProductFromFavorite(this.product.id).subscribe(item=>{
        if(item){
          this.favorite=true
        }
      });
    }
  }
  addToFavorite(event:any) {
    event.preventDefault();
    event.stopPropagation();
    this.favorite = !this.favorite;
    if(this.favorite && this.product){
      this.productService.addProductToFavorite(this.product.id).subscribe((item)=>{
        this.product ? this.productService.productFavoredChange.next({product:this.product,changeStatus:productFavoredChangeStatus.ADD}):null
      })
    }
    else if(!this.favorite && this.product){
      this.productService.deleteProductFromFavorite(this.product.id).subscribe((item)=>{
        this.product ? this.productService.productFavoredChange.next({product:this.product,changeStatus:productFavoredChangeStatus.DELETE}):null
      })
    }
  }

  getOffer(){
    return this.product?.hasOffer ? this.product.totalOffers + '%' : ''
  }
  addToCart(event:any){
    event.preventDefault();
    event.stopPropagation();
    this.productCartService.addProductToCart({
      color:'',
      id: this.product?.id  ?? 1,
      imageUrl: this.product?.mainPhoto.url ?? '',
      name: this.product?.name ?? '',
      price: this.product?.price ?? 0,
      quantity: 1,
      size: 'XL',
      totalPrice: this.product?.price
    })
    this.snackBar.open('تم إضافة المنتج الى السلة', 'حسناً', {
      duration: 3000,
    });
    
    
  }
}
