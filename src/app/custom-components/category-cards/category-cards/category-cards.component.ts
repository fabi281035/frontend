import { Component, Input } from '@angular/core';
import { CategoriesService } from 'src/app/services/categories.service';
import { Category } from 'src/models/category';

@Component({
  selector: 'app-category-cards',
  templateUrl: './category-cards.component.html',
  styleUrls: ['./category-cards.component.scss']
})
export class CategoryCardsComponent {
  @Input() categories$ =this.categoriesService.getCategory();
  constructor(private categoriesService:CategoriesService){}
}
