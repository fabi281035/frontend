import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeTimerComponent } from './time-timer/time-timer.component';



@NgModule({
  declarations: [
    TimeTimerComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TimeTimerComponent
  ],
})
export class TimeTimerModule { }
