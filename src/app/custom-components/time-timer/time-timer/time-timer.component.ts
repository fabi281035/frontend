import { Component, Input, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-time-timer',
  templateUrl: './time-timer.component.html',
  styleUrls: ['./time-timer.component.scss']
})
export class TimeTimerComponent implements OnInit, OnDestroy {
  @Input() finishTime: string = '2023-09-24';
  displayTime: string = '';

  private timer: any;
  private startTime: number = 0;
  private elapsedTime: number = 0;
  private running: boolean = false;

  constructor() { }

  ngOnInit() {
    this.calculateRemainingTime();
    this.startTimer();
  }

  ngOnDestroy() {
    this.stopTimer();
  }

  startTimer() {
    if (!this.running) {
      this.startTime = Date.now() - this.elapsedTime;
      this.timer = setInterval(() => {
        this.elapsedTime = Date.now() - this.startTime;
        this.calculateRemainingTime();
        if (this.displayTime === '00:00:00') {
          this.stopTimer();
        }
      }, 1000);
      this.running = true;
    }
  }

  stopTimer() {
    clearInterval(this.timer);
    this.running = false;
  }

  resetTimer() {
    this.stopTimer();
    this.elapsedTime = 0;
    this.calculateRemainingTime();
  }

  private calculateRemainingTime() {
    const finishDateTime = new Date(this.finishTime);
    const remainingTime = Math.max(0, finishDateTime.getTime() - Date.now());
    const totalSeconds = Math.floor(remainingTime / 1000);
    const hours = Math.floor(totalSeconds / 3600);
    const minutes = Math.floor((totalSeconds % 3600) / 60);
    const seconds = Math.floor(totalSeconds % 60);

    const formattedHours = this.padNumber(hours);
    const formattedMinutes = this.padNumber(minutes);
    const formattedSeconds = this.padNumber(seconds);

    this.displayTime = `${formattedHours}:${formattedMinutes}:${formattedSeconds}`;
  }

  private padNumber(number: number): string {
    return number.toString().padStart(2, '0');
  }
}