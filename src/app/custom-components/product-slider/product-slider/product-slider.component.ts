import { Component, OnInit, Input, ViewChild, ElementRef, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/models/product';

@Component({
  selector: 'app-product-slider',
  templateUrl: './product-slider.component.html',
  styleUrls: ['./product-slider.component.scss']
})
export class ProductSliderComponent implements OnInit{
  @Input() cardSize: number=300;
  @Input() title: string='العروض الحالية';
  @Input() unitDate: string='';
  @Input() hasBackground: boolean=false;
  @Input() products: Product[]=[];
  @Input() showMoreButton: boolean = false;
  @Input() brandId: number = 0

  public elementWidthInPX:number=1000;

  currentIndex: number = 0;

  constructor(private router: Router) { }

  ngOnInit() {
  }


  @ViewChild('sliderContainer', { static: false }) sliderContainer: ElementRef | undefined;

  ngAfterViewInit() {
    this.elementWidthInPX = this.sliderContainer?.nativeElement.offsetWidth;
  }

  nextCard() {
    if (this.currentIndex < this.products.length - 1) {
      this.currentIndex++;
      this.currentIndex=this.currentIndex<=this.elementWidthInPX/this.cardSize? this.currentIndex:0
    }
  }

  prevCard() {
    if (this.currentIndex > 0) {
      this.currentIndex--;
    }
  }

  productFilter(){
    this.router.navigateByUrl(`/product/category/${this.products[0].productCategory.id}/brand/${this.brandId}`);
  }
}
