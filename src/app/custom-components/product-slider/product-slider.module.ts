import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductSliderComponent } from './product-slider/product-slider.component';
import { ProductCardModule } from '../product-card/product-card.module';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TimeTimerModule } from '../time-timer/time-timer.module';



@NgModule({
  declarations: [
    ProductSliderComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    ProductCardModule,
    TimeTimerModule,
  ],
  exports:[
    ProductSliderComponent
  ]
})
export class ProductSliderModule { }
